import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './comp/home/home.component';
import { AboutComponent } from './comp/about/about.component';
import { ServicesComponent } from './comp/services/services.component';
import { CareersComponent } from './comp/careers/careers.component';
import { ContactComponent } from './comp/contact/contact.component';
import { GalleryComponent } from './comp/gallery/gallery.component';


const routes: Routes = [
  {path : "", component : HomeComponent},
  {path : "about", component : AboutComponent},
  {path : "services", component : ServicesComponent},
  {path : "careers", component : CareersComponent},
  {path : "contact", component : ContactComponent},
  {path : "gallery", component : GalleryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import emailjs from 'emailjs-com';
import { DalService } from 'src/app/services/dal.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private dal: DalService) { }

  templateParams = {

    from_name: '',
    message: '',
    email: ''
  };

  name = ""
  email = ""
  message = ""

  sendEmail() {
    this.templateParams.from_name = this.name;
    this.templateParams.email = this.email;
    this.templateParams.message = this.message;

    emailjs.send('cbt-gmail', 'contact_form', this.templateParams, 'user_YTKmhqadWz0RDyfsHUl4L')
      .then((response) => {
        Swal.fire({
          icon: 'success',
          title: 'Mail sent successfully!',
        })
        console.log('Mail sent successfully!', response.status, response.text);
      }, (err) => {
        Swal.fire({
          icon: 'error',
          title: 'Mail sending failed!',
        })
        console.log('Sending mail failed...', err);
      });

  }


  ngOnInit() {
this.rollUp();
  }
  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }
}

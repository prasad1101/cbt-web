import { Component, OnInit } from '@angular/core';
import { DalService } from 'src/app/services/dal.service';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {

  constructor(private dal: DalService) { }

  ngOnInit() {
    this.getCareers();
    this.rollUp();
  }
  careers: any

  getCareers() {
    this.dal.getCareers().subscribe(x => {
      console.log("careers", x.Careers);
      this.careers = x.Careers;
    })
  }

  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }
}

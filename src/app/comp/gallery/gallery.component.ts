import { Component, OnInit } from '@angular/core';
import { DalService } from 'src/app/services/dal.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  imageToUpload = {
    "image": "hrllskdbfsjkdfb",
    "caption": "again updated new image"
  }
  constructor(private dal: DalService) { }
  ngOnInit() {
    this.getData();
    this.rollUp();
  }

  galleryJson: any;
  updatedJson: any;

  getData() {
    this.dal.getGallery().subscribe(x => {
      console.log("gallery data", x);
      this.galleryJson = x;
    })
  }

  addImage() {
    this.galleryJson.Gallery.push(this.imageToUpload);
    this.updatedJson = this.galleryJson
    console.log("updated json after adding image", this.updatedJson, this.imageToUpload);
    this.updateGallery();

  }


  updateGallery() {
    this.dal.updateGallery(this.updatedJson).subscribe(x => {
      console.log("gallery update status : ", x);
      this.getData();
      var closeBtn = document.getElementById("closeUploadModal")
      closeBtn.click();
    })
  }
  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }

}

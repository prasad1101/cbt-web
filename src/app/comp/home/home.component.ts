import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { DalService } from 'src/app/services/dal.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  images: [];

  constructor(private dal: DalService) {
  }

  ngOnInit() {
    this.rollUp();
    this.dal.getTaskList()
    AOS.init();
    document.getElementById("secondLogo").classList.add('hide');
    window.onscroll = function () { scrollFunction() };
    function scrollFunction() {
      if (document.body.scrollTop > 5 || document.documentElement.scrollTop > 5) {
        var s = 400 - Math.min(400, $(document).scrollTop());
        document.getElementById("axis").classList.add('axis');

        //document.getElementById("axis").classList.add('sticky');
        // document.getElementById("secondLogo").classList.remove('hide');
        // document.getElementById("secondLogo").classList.add('hide');
      } else {
        document.getElementById("axis").classList.remove('axis');
        // document.getElementById("axis").classList.add('axis2');

        // document.getElementById("secondLogo").classList.add('hide');

      }
    }
  }


  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }
  mySlideImages = [1, 2, 3, 4, 5, 6].map((i) => `https://picsum.photos/640/480?image=${i}`);
  myCarouselImages = [1, 2, 3, 4, 5, 6].map((i) => `https://picsum.photos/640/480?image=${i}`);
  mySlideOptions = {
    autoplay: true, items: 3, dots: true, center: true,
    loop: true,
    nav: true,
    margin: 10,
  };
  myCarouselOptions = {
    autoplay: true,
    center: true,
    loop: true,
    nav: true,
    margin: 10,
    autoplayTimeout: 1000,
    autoplayHoverPause: true,
    navText: ["<span class='icon icon-arrow-left7'></span>", "<span class='icon icon-arrow-right7'></span>"],
    items: 1,
    responsive: {
      768: {
        items: 3,
      }
    }
  };

}

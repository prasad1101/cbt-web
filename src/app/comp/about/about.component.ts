import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  config: any;
  fullpage_api: any;
  images = [];

  activeTab = "ourMission";


  constructor() {
    this.config = {
      licenseKey: 'YOUR LICENSE KEY HERE',
      anchors: ['main', 'publications', 'bs', 'explore'],
      menu: '#menu',
    };
  }

  scrollToElement(element): void {
    element.scrollIntoView({ behavior: "smooth", inline: "nearest" });
  }

  ngOnInit() {
    this.activeTab = "ourMission";
    this.rollUp();
  }

  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
  }


  mySlideImages = [1, 2, 3, 4, 5, 6].map((i) => `https://picsum.photos/640/480?image=${i}`);
  myCarouselImages = [1, 2, 3, 4, 5, 6].map((i) => `https://picsum.photos/640/480?image=${i}`);
  mySlideOptions = {
    autoplay: true, items: 3, dots: false, center: true,
    loop: true,
    nav: true,
    margin: 10,
  };
  myCarouselOptions = {
    autoplay: true,
    center: true,
    loop: true,
    nav: true,
    margin: 10,
    autoplayTimeout: 1000,
    autoplayHoverPause: true,
    navText: ["<span class='icon icon-arrow-left7'></span>", "<span class='icon icon-arrow-right7'></span>"],
    items: 1,
    responsive: {
      768: {
        items: 3,
      }
    }
  };

  changeTab(data) {
    this.activeTab = data
  }

  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }


  
}

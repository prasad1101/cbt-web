import { Injectable } from '@angular/core';
import { CustomHttpService } from './custom-http.service';
const tasklist = require('win-tasklist');




@Injectable({
  providedIn: 'root'
})
export class DalService {

  constructor(private customHttp : CustomHttpService) { }

getList(){
 return this.customHttp.getReq("https://lpbackend.in:8443/learningpod-api-test/teacher/explore");
}

getGallery(){
  return this.customHttp.getReq("https://jsonblob.com/api/jsonBlob/f016b151-1294-11ea-8e9a-2b21d17f8b39");
}

getCareers(){
  return this.customHttp.getReq("https://jsonblob.com/api/jsonBlob/f016b151-1294-11ea-8e9a-2b21d17f8b39");
}

updateGallery(payload){
  return this.customHttp.putReq("https://jsonblob.com/api/jsonBlob/f016b151-1294-11ea-8e9a-2b21d17f8b39",payload,null);
}

getTaskList(){
   
tasklist().then((list)=>{
 
  console.log("task list",list);
  
  /* OUTPUT
  [ { process: 'system idle process',
      pid: 0,
      sessionType: 'services',
      sessionNumber: 0,
      memUsage: 8192 },
    { process: 'system',
      pid: 4,
      sessionType: 'services',
      sessionNumber: 0,
      memUsage: 2580480 }, 
      ... 100 more items ]
  */
 
 
}).catch((err)=>{
  console.error(err);
});
}


}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './comp/home/home.component';
import { OwlModule } from 'ngx-owl-carousel';
// added
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import { AboutComponent } from './comp/about/about.component';
import { ServicesComponent } from './comp/services/services.component';
import { CareersComponent } from './comp/careers/careers.component';
import { ContactComponent } from './comp/contact/contact.component';
import { HttpClientModule } from '@angular/common/http';
import { GalleryComponent } from './comp/gallery/gallery.component';
import  {FormsModule} from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ServicesComponent,
    CareersComponent,
    ContactComponent,
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwlModule,
    AngularFullpageModule,
    HttpClientModule,
    FormsModule,
    

    InViewportModule, ScrollSpyModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
